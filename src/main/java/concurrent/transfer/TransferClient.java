package concurrent.transfer;

import org.apache.commons.codec.binary.Hex;

import java.io.*;
import java.net.Socket;
import java.security.DigestException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Date;


public class TransferClient {
    private Socket socket;
    private long contentLen;
    private long received;
    private OutputStream outputStream;
    private Date beginTime;
    private Date endTime;
    private byte[] buffer = new byte[Config.MAX_BUFFER];
    private byte[] hash;
    private String filePath;
    private MessageDigest messageDigest;
    private boolean readHeader = false;
    public static long boundary = 8292902600220228L;
    public static String OK_MSG = "OK";
    public static String HASH_ERROR = "HASH ERROR";
    public static String SERVER_ERROR = "SERVER ERROR";



    public boolean isReadHeader() {
        return readHeader;
    }


    public long getReceived() {
        return received;
    }

    public String getFilePath() {
        return filePath;
    }




    public OutputStream getOutputStream() {
        return outputStream;
    }

    public Date getBeginTime() {
        return beginTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void readWrite() throws IOException, DigestException {
        int actualread = socket.getInputStream().read(buffer);
        received = received + actualread;
        outputStream.write(buffer,0,actualread);
        messageDigest.update(buffer,0,actualread);
        //System.out.println("\n"+String.format("Filename '%s' size '%d' hash '%s' :: received %d bytes",filePath,contentLen, Hex.encodeHexString(hash), received));
        outputStream.flush();

    }




    public TransferClient(Socket socket) {
        this.socket = socket;

        try {
            messageDigest = MessageDigest.getInstance(Config.CHKSUM);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    public Socket getSocket() {
        return socket;
    }

    public long getContentLen() {
        return contentLen;
    }


    public void readHeader() throws IOException, CorruptedTransferException {
        DataInputStream dataInputStream = new DataInputStream(socket.getInputStream());
        long start = dataInputStream.readLong();
        if(start!=boundary){
            throw new CorruptedTransferException("Unexpected data. Aborting");
        }
        contentLen = dataInputStream.readLong();
        filePath = dataInputStream.readUTF();
        hash = new byte[Config.HASHLEN];
        dataInputStream.readFully(hash);
        System.out.println("\n"+String.format("Filename '%s' size '%d' hash '%s'",filePath,contentLen, Hex.encodeHexString(hash)));
        long border = dataInputStream.readLong();
        if(border!=boundary){
            throw new CorruptedTransferException("Unexpected data. Aborting");
        }

        File directorystore = new File(Config.FILE_DIRECTORY);
        if(!directorystore.exists())directorystore.mkdir();
        File file = new File(Config.FILE_DIRECTORY + filePath);
        if(!file.exists())file.createNewFile();

        outputStream = new BufferedOutputStream(new FileOutputStream(file));
        beginTime = new Date();
        readHeader = true;


    }

    public void kill() throws IOException {
        if(socket != null)socket.getInputStream().close();
        if(outputStream != null)outputStream.close();

    }

    public boolean verify(){
        endTime = new Date();
        byte[] hashv = messageDigest.digest();
        System.out.println(String.format("file '%s' size %d hash = '%s' calculated hash = '%s'",filePath,contentLen,Hex.encodeHexString(hash),Hex.encodeHexString(hashv)));
        return Arrays.equals(hash,hashv);

    }

     public void sendResponse(String message) throws IOException {
         DataOutputStream dataInputStream = new DataOutputStream(socket.getOutputStream());
         dataInputStream.writeUTF(message);
         socket.getOutputStream().flush();


     }



}
