package concurrent.transfer;

public class Config {
    public static final int SERVER_PORT = 5053;
    public static final int MAX_THREADS = 16;
    public static final int MAX_BUFFER = 10240;
    public static final String CHKSUM = "SHA1";
    public static final int HASHLEN = 20;
    public static final String FILE_DIRECTORY = "files/";
}
