package concurrent.transfer;

public class CorruptedTransferException extends Exception {

    public CorruptedTransferException(String message){
        super(message);
    }

}
