package concurrent.transfer;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;



public class ServerApplication {
    private List<ClientConnectionThread> threads = new ArrayList<>(Config.MAX_THREADS);
    private int cycle = 0;

    public void run(){


        ServerSocket serverSocket = null;
        try {
            serverSocket = new ServerSocket(Config.SERVER_PORT);
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }
        while (true) {

            try {
                Socket socket = serverSocket.accept();
                TransferClient transferClient = new TransferClient(socket);
                if(threads.size()<Config.MAX_THREADS){
                    ClientConnectionThread newCCThread = new ClientConnectionThread();
                    newCCThread.addClient(transferClient);
                    Thread thread = new Thread(newCCThread);
                    threads.add(newCCThread);
                    thread.start();
                }else {
                    threads.get(cycle).addClient(transferClient);
                    cycle = (cycle+1)%Config.MAX_THREADS;
                }

            }catch (IOException | CorruptedTransferException e){
                e.printStackTrace();

            }



        }
        //clear threads



    }

}
