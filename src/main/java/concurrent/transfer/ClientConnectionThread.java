package concurrent.transfer;

import java.io.IOException;
import java.security.DigestException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class ClientConnectionThread implements Runnable {
    private List<TransferClient> transferClients = new CopyOnWriteArrayList<>(new ArrayList<>());

    public ClientConnectionThread(){

    }

    @Override
    public void run() {
        while(true){
            if(transferClients.isEmpty())
                Thread.yield();
            for(TransferClient transferClient:transferClients){

                try {
                    if(transferClient.getSocket().getInputStream().available()>0){
                        if(!transferClient.isReadHeader()){
                            transferClient.readHeader();
                        }else {
                            transferClient.readWrite();
                            if(transferClient.getReceived()>=transferClient.getContentLen()){
                                if(transferClient.verify()){
                                    transferClient.sendResponse(TransferClient.OK_MSG);
                                }else {
                                    transferClient.sendResponse(TransferClient.HASH_ERROR);
                                    
                                }
                                transferClient.kill();
                                removeClient(transferClient);


                            }
                        }

                    }

                }catch(IOException ioExp){
                    try {
                        ioExp.printStackTrace();
                        transferClient.kill();
                        removeClient(transferClient);

                    }catch(IOException exp){
                        exp.printStackTrace();

                    }
                    removeClient(transferClient);


                }catch(CorruptedTransferException exp){
                    exp.printStackTrace();

                } catch (DigestException e) {
                    e.printStackTrace();
                }


            }

        }

    }

    public void addClient(TransferClient transferClient) throws IOException, CorruptedTransferException {
        transferClients.add(transferClient);

    }

    public void removeClient(TransferClient transferClient){
        transferClients.remove(transferClient);
    }

}
